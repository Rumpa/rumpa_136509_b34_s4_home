<?php
//Compare two strings
echo "substr_compare:";
echo substr_compare("Hello world","Hello world",0);//return 0 if the both strings are equal
echo "<br>";
echo substr_compare("Hello world","Hello word",0);//return >0 that is 1 if string1 (from startpos) is greater than string2
echo "<br>";
echo substr_compare("Hello wold","Hello world",0);//return <0 that is -1 if string1 (from startpos) is less than string2

?>