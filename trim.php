<?php
//Remove characters from both sides of a string
$str="Hello World!";
echo $str."<br>";
echo "trim:";
echo trim($str,"Hed!");
?>


<?php
//ltrim(Remove characters from the left side of a string)
echo "<br>";
$str="Hello World!";
echo $str."<br>";
echo "ltrim:";
echo ltrim($str,"Hello");
?>

<?php
echo "<br>";
$str="Hello World!";
echo $str."<br>";
echo "rtrim:";
echo rtrim($str,"World!");
?>

<?php
echo "<br>";
$str="Hello Wor\nld!";
echo $str."<br>";

$str1=trim($str);
echo $str1;
?>

<?php
echo "<br>";
$a="   \t this is count whitespace tab";
echo $a."<br>";
$b=trim($a);
echo "trim:";
echo $b;


?>